'''
Module for monitoring RefTek stations and sending SOH data to Zabbix.
Created on Jun 29, 2020
@author: waaq
'''

import sys
import os
import logging
import logging.config
from soh_datalogger import soh_utils, soh_reftek

# Initialize logging
def initialize_logging():
    """Initialize logging configuration."""
    config_path = os.path.join(os.path.dirname(__file__), 'config', 'logging.ini')
    if os.path.exists(config_path):
        logging.config.fileConfig(config_path, disable_existing_loggers=False)
    else:
        raise FileNotFoundError(f"Logging configuration file not found: {config_path}")
    return logging.getLogger(__name__)

def main():
    """
    Parses the configuration file, sets parameters, and reads RefTek stations.
    """
    logger = initialize_logging()

    if len(sys.argv) != 2:
        logger.error(f"Invalid number of arguments. Usage: python {sys.argv[0]} CONFIGURATION_FILE.txt")
        print(f"Usage: python {sys.argv[0]} CONFIGURATION_FILE.txt")
        sys.exit(1)

    config_file = sys.argv[1]
    try:
        logger.info("Reading configuration file.")
        run_param = soh_utils.read_parameters(config_file)
        logger.info("Configuration file successfully read.")
    except Exception as e:
        logger.error(f"Error reading configuration file: {e}", exc_info=True)
        sys.exit(1)

    try:
        logger.info("Extracting parameters from the configuration.")
        reftek_monitor_url = run_param['REFTEK_INFO']['web_url']
        reftek_station_file = run_param['REFTEK_INFO']['file_path']
        zabbix_server = run_param['ZABBIX_INFO']['ip']
        zabbix_port = run_param['ZABBIX_INFO']['port']
        logger.info("Parameters successfully extracted from the configuration.")
    except Exception as e:
        logger.error(f"Missing required parameter in configuration file: {e}", exc_info=True)
        sys.exit(1)

    try:
        logger.info("Reading RefTek station file.")
        reftek_ips = soh_utils.read_config_file(reftek_station_file)
        logger.info("RefTek station file successfully read.")
    except Exception as e:
        logger.error(f"Error reading RefTek station file: {e}", exc_info=True)
        sys.exit(1)

    try:
        logger.info("Connecting to Zabbix server.")
        zabbix_sender = soh_reftek.connect2zabbix(zabbix_server, int(zabbix_port))
        logger.info("Successfully connected to Zabbix server.")

        logger.info("Fetching SOH data from RefTek monitoring URL.")
        soh_datas = soh_reftek.get_reftek_soh(reftek_monitor_url)
        logger.info("SOH data successfully fetched.")

        logger.info("Determining the status of RefTek stations.")
        rk_stat = soh_reftek.reftek_status(reftek_ips)
        logger.info("RefTek station status successfully determined.")

        logger.info("Sending SOH data to Zabbix.")
        for soh_data in soh_datas:
            soh_reftek.send2zabbix(zabbix_sender, soh_data, rk_stat)
        logger.info("Successfully sent SOH data to Zabbix.")
    except Exception as e:
        logger.error(f"Error processing RefTek SOH data: {e}", exc_info=True)
        sys.exit(1)



if __name__ == "__main__":
    main()





