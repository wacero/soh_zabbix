'''
Created on Jun 29, 2020

@author: wacero
'''

import time
from bs4 import BeautifulSoup
import requests
from pyzabbix import ZabbixMetric, ZabbixSender
import os, sys
from soh_datalogger import soh_utils
from soh_datalogger import soh_quanterra
import logging, logging.config
import subprocess
import re

logging.config.fileConfig('./logging.ini', disable_existing_loggers=False)
logger=logging.getLogger('soh_zabbix')

def main():
    
    """
    This function parse the configuration file, set parameters and read reftek stations
    """
     
    is_error = False
    
    if len(sys.argv)==1:
        is_error=True
    
    else:
        try:
            run_param=soh_utils.read_parameters(sys.argv[1])
        except Exception as e:
            logger.error("Error reading configuration file: %s" %str(e))
            raise Exception("Error reading configuration file: %s" %str(e))
    
        try:
           
            q330_station_file=run_param['Q330_INFO']['file_path']
            q330_http_port=run_param['Q330_INFO']['port']
            zabbix_server=run_param['ZABBIX_INFO']['ip'] 
            zabbix_port =run_param['ZABBIX_INFO']['port']
        except Exception as e:
            logger.error("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))    
    
        try:
            q330_ips=soh_utils.read_config_file(q330_station_file)
            
            
        except Exception as e:
            logger.error("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))

        try:
            zx_server=connect2zabbix(zabbix_server, int(zabbix_port))
            
        except Exception as e:
            logger.error("Error in main(): error getting zabbix connection %s " %str(e))


        try:
            
            #Llamar en serie y luego paralelizar
            for station,ip in q330_ips.items():
                print(station,ip)
                soh_quanterra.check_get_http(station,ip,q330_http_port,zx_server)

        except Exception as e:
            print("Error in check_get_http, was :%s" %(str(e)) )

    if is_error:
        logger.info(f'Usage: python {sys.argv[0]} configuration_file.txt ')
        print(f'Usage: python {sys.argv[0]} CONFIGURATION_FILE.txt ')




main()




#print(soh_data)





