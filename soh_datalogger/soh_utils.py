'''
Created on Jul 3, 2020

@author: seiscomp
'''
import json
import logging
import configparser
import csv
from pyzabbix import ZabbixSender
import subprocess


def check_host_up(host_ip):
    """
    Check if the ip host is up
    
    :param string host_ip
    :return booleans 
    """
    try:
        response = subprocess.run(["ping","-c 1","%s" %host_ip ],stdout=subprocess.DEVNULL,timeout=25)
        return_code=response.returncode
        
    except: 
        return_code = -1	
        #pass

    if return_code == 0:
        return True
    else:
        return False


def get_zabbix_sender(zabbix_ip,zabbix_port):
    """
    Try to connect to a ZX server, return
    
    :param string zabbix_ip
    :param string zabbix_port
    :raises: throws exception if unable to connect to zabbix server
    """
    try:
        return ZabbixSender(zabbix_ip,zabbix_port)
        
    except Exception as e:
        raise("Error in connect2zabbix: %s" %str(e))    


def read_config_file(json_file_path):
    """
    Reads a json_file and returns it as a python dict
    
    :param string json_file_path: path to a json file with configuration information
    :returns: dict
    """
    
    json_file=check_file(json_file_path)
    with open(json_file) as json_data:
        return json.load(json_data)
    

def read_parameters(file_path):
    """
    Read a configuration text file
    
    :param string file_path: path to configuration text file
    :returns: dict: dict of a parser object
    """
    parameter_file=check_file(file_path)
    parser=configparser.ConfigParser()
    parser.read(parameter_file)    
    return parser._sections


def check_file(file_path):
    '''
    Check if the file exists
    
    :param string file_path: path to file to check
    :return: file_path
    :raises Exception e: General exception if file doesn't exist. 
    '''
    try:
        
        with open(file_path):
            return file_path

    except Exception as e:
        logging.error("Error in check_file(%s). Error: %s " %(file_path,str(e)))
        raise Exception("Error in check_file(%s). Error: %s " %(file_path,str(e)))
    

def create_hosts(lista_csv):
    """ 
    create a json file

    :param list lista_csv
    """
    with open(lista_csv,newline='') as csvfile:
        station_list = csv.reader(csvfile,delimiter=',')
        for row in station_list:
            station,channel,IP,val1,val2 = row
            print("\"%s\": \"%s\"," %(station,IP))
        

