import requests
import time
import logging
from pyzabbix import ZabbixMetric, ZabbixSender
from bs4 import BeautifulSoup
import re
import subprocess

def connect2zabbix(zabbix_ip,zabbix_port):
    """
    Try to connect to a ZX server, return
    
    :param string zabbix_ip
    :param string zabbix_port
    :raises: throws exception if unable to connect to zabbix server
    """
    try:
        return ZabbixSender(zabbix_ip,zabbix_port)
        
    except Exception as e:
        raise("Error in connect2zabbix: %s" %str(e))    

def get_reftek_soh(url):
    """
    Download reftek soh web and return all stations
    
    :param string url 
    :return soh_data:
    """

    html_content = requests.get(url).text
    soup = BeautifulSoup(html_content, "html.parser")
    tables = soup.find_all("table")
    rows = tables[1].find_all("tr")
    soh_data=[]
    
    for row in rows:
        cols=row.find_all("td")
        cols = [element.text.strip() for element in cols]
        
        station=cols[1]
        temperature=cols[4].rstrip("°C")
        voltage = cols[5].rstrip("V")
        voltage_backup=cols[6].rstrip("V")
        delay = cols[7]
        ram = cols[8].split("%",1)[0]
        disk_1= cols[9].split("%",1)[0]
        disk_2 =  cols[10].split("%",1)[0]
        data= [station,temperature,voltage , voltage_backup, delay,ram,disk_1,disk_2]
        
        soh_data.append(data)
    
    return soh_data[1:]



def convert2float(valor):

    numero, unidad = re.match(r'([0-9.]+)(\D+)', valor).groups()

    numero = float(numero)

    if unidad == 'ms':
        return numero * 1e-3  # milisegundos a segundos
    elif unidad == 'µs':
        return numero * 1e-6  # microsegundos a segundos
    else:
        #raise ValueError(f"Unidad no reconocida: {unidad}")
        return -1




def get_system_value_simple(soup, title):
    element = soup.find('p', title=title)
    if element:
        return element.strong.text.strip()
    return None


def get_system_value_regex(soup, pattern):
    # Usar una expresión regular para buscar el título
    element = soup.find('p', attrs={'title': lambda x: x and re.search(pattern, x)})
    if element:
        return element.strong.text.strip()
    return None


def get_guralp_soh(station,guralp_ip):

    ##ASLC, 217.21 tiene hasta box 7 !!
    ## BRRN, 16.184 tiene usuario, contraseña 
    ## AIB2, 192.168.16.18, 
    ## IESS, 192.168.17.108, retardo de 46 segundos
    ## FENY, 192.168.17.107, fuera sin ping
    ## cusw, 192.168.16.77, problema en web
    """
    Download guralp soh web and return all stations
    
    :param string url 
    :return soh_data:
    """
    try: 
        url = f"http://{guralp_ip}/cgi-bin/xmlstatus.cgi#box8"
        html_content = requests.get(url, timeout=36).text
        soup = BeautifulSoup(html_content, 'html.parser')
        voltage = get_system_value_simple(soup,"system.systemvoltage")
        temperature = get_system_value_simple(soup,"system.temperature")



        voltage = re.search(r"[-+]?\d*\.?\d+", voltage).group()
        temperature = re.search(r"[-+]?\d*\.?\d+", temperature).group()

        url_5 = f"http://{guralp_ip}/cgi-bin/xmlstatus.cgi#box5"
        html_content = requests.get(url_5, timeout=36).text
        soup = BeautifulSoup(html_content, 'html.parser')

        clock_difference = get_system_value_regex(soup,".*clock_diff")
        time_locked = get_system_value_simple(soup,"ntp.locked")
        error_time = get_system_value_simple(soup,"ntp.estimated_error")    

        error_time = convert2float(error_time)
        clock_difference = convert2float(clock_difference)

        logging.info(f"SOH values for {station} were: {voltage,temperature,time_locked,error_time} ")
        print("##RESULT")
        print(station,voltage,temperature,clock_difference)

        return dict({ "host":station,  "voltage":voltage, "temperature":temperature,"clock_difference":clock_difference })

    except Exception as e:
        logging.info(f"Error in get_guralp_soh: {station} was {e}")
        return None





def check_guralp_up(hostname):
    """
    Check if the ip function is up
    
    :param string hostname
    :return booleans 
    """
    print("check status")
    try:
        response = subprocess.run(["ping","-c 1","%s" %hostname ],stdout=subprocess.DEVNULL,timeout=36)
        return_code=response.returncode
        
    except: 
        return_code = -1	
        pass

    if return_code == 0:
        return True
    else:
        return False


def guralp_status(guralp_dict):
    """ 
    Create a dictionary of station states
    
    :param dict guralp_dict
    :return guralp_status_dict
    """
    guralp_status_dict = {}
    for rk,ip in guralp_dict.items():
        
        guralp_status_dict.update({rk:check_guralp_up(ip)})
        
    logging.info(guralp_status_dict)
    return guralp_status_dict


def send2zabbix(zabbix_server,host_soh):
    """
    Receive soh_data array and send it to a ZX trigger item only  if ping ok 

    :param string zabbix_server
    :param string soh_data
    :param dict rk_stat
    :Exception: error sending information to zabbix server
    """
    
    try:
        
        metrics = [
            ZabbixMetric(host_soh['host'],'guralp_voltage',float(host_soh['voltage'])),
            ZabbixMetric(host_soh['host'],'guralp_temperature',float(host_soh['temperature'])),
            ZabbixMetric(host_soh['host'],'guralp_clock_diff',float(host_soh['clock_difference']))

        
        ]

    except Exception as e:
        logging.info(f"Error while creating the metrics: {e}")

    try:
        result = zabbix_server.send(metrics)
        logging.info(f"Result of sending metrix for {host_soh['host']} was :{result}")


    except Exception as e:
        logging.info(f"Error while sending the metrics: {e}")



    """
    try:
        hostname = "%s" %soh_data[0]
        if rk_stat[hostname]==True:

            hostname="%s" %soh_data[0]
            metrics=[
                ZabbixMetric(hostname,'reftek_temperature',float(soh_data[1])),
                ZabbixMetric(hostname,'reftek_voltage',float(soh_data[2]),time.time()),
                ZabbixMetric(hostname,'reftek_voltage_intern',float(soh_data[3]),time.time()),
                ZabbixMetric(hostname,'reftek_delay',float(soh_data[4]),time.time()),
                ZabbixMetric(hostname,'reftek_disk_1',float(soh_data[6]),time.time()),
                ZabbixMetric(hostname,'reftek_disk_2',float(soh_data[7]),time.time())
                ]
  
            try:
                res=zabbix_server.send(metrics)
                logging.info("%s, %s" %(hostname,res))
            except Exception as e:
                logging.info("Error in send2zabbix. Error was: %s" %str(e))
   
    except Exception as e:
        logging.info("Not station in dictionary: %s" %str(e))
    
    """