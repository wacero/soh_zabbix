import requests
import time
import logging
from pyzabbix import ZabbixMetric, ZabbixSender
from bs4 import BeautifulSoup
import subprocess


def connect2zabbix(zabbix_ip, zabbix_port):
    """
    Try to connect to a ZX server and return a ZabbixSender object.

    :param string zabbix_ip: Zabbix server IP
    :param string zabbix_port: Zabbix server port
    :raises Exception: Throws exception if unable to connect to Zabbix server
    :return: ZabbixSender object
    """
    try:
        return ZabbixSender(zabbix_ip, zabbix_port)
    except Exception as e:
        raise Exception(f"Error in connect2zabbix: {e}")



def get_reftek_soh(url):
    """
    Download RefTek SOH web page and return all station data.

    :param string url: RefTek monitoring URL
    :return list soh_data: List of SOH data for all stations
    """

    html_content = requests.get(url).text
    soup = BeautifulSoup(html_content, "html.parser")
    tables = soup.find_all("table")
    rows = tables[1].find_all("tr")
    soh_data = []
    
    for row in rows:
        cols = row.find_all("td")
        cols = [element.text.strip() for element in cols]
        
        station = cols[1]
        temperature = cols[4].rstrip("°C")
        voltage = cols[5].rstrip("V")
        voltage_backup = cols[6].rstrip("V")
        delay = cols[7]
        ram = cols[8].split("%",1)[0]
        disk_1 = cols[9].split("%",1)[0]
        disk_2 =  cols[10].split("%",1)[0]
        data = [station,temperature,voltage , voltage_backup, delay,ram,disk_1,disk_2]
        
        soh_data.append(data)
    
    return soh_data[1:]

def check_reftek_up(hostname):
    """
    Check if the RefTek station is reachable via ping.

    :param string hostname: IP address
    :return bool: True (1) if the station is reachable, otherwise False
    """
    try:
        #response = subprocess.run(["ping","-c 1","%s" %hostname ],stdout=subprocess.DEVNULL,timeout=2)
        response = subprocess.run(
            ["ping", "-c", "1", hostname],
            stdout=subprocess.DEVNULL,
            timeout=2
        )
        return_code = response.returncode
        
    except: 
        return_code = -1	
        
    return return_code

def reftek_status(reftek_dict):
    """ 
    Create a dictionary of station states.

    :param dict reftek_dict: Dictionary of station names and their IPs
    :return dict reftek_status_dict: Dictionary of station names and their statuses
    """
    reftek_status_dict = {}
    for rk, ip in reftek_dict.items():
        reftek_status_dict[rk] = check_reftek_up(ip)
    logging.info(f"RefTek Status: {reftek_status_dict}")
    return reftek_status_dict


def send2zabbix(zabbix_sender,soh_data,rk_stat):

    """
    Send SOH data to Zabbix server if the station is reachable.

    :param ZabbixSender zabbix_sender: ZabbixSender object
    :param list soh_data: SOH data of a station
    :param dict rk_stat: Dictionary of station statuses
    :raises Exception: Error sending information to Zabbix server
    """
    try:
        hostname = f"{soh_data[0]}"
        if rk_stat[hostname]==0:
            metrics=[
                ZabbixMetric(hostname,'reftek_temperature',float(soh_data[1])),
                ZabbixMetric(hostname,'reftek_voltage',float(soh_data[2]),time.time()),
                ZabbixMetric(hostname,'reftek_voltage_intern',float(soh_data[3]),time.time()),
                ZabbixMetric(hostname,'reftek_delay',float(soh_data[4]),time.time()),
                ZabbixMetric(hostname,'reftek_disk_1',float(soh_data[6]),time.time()),
                ZabbixMetric(hostname,'reftek_disk_2',float(soh_data[7]),time.time())
                ]
  
            try:
                res = zabbix_sender.send(metrics)
                logging.info(f"Metrics sent for {hostname}: {res}")
            except Exception as e:
                logging.info("Error in send2zabbix. The error was: {e}" )
        
        else:
            logging.warning(f"Station {hostname} is not reachable.")
   
    except Exception as e:
        logging.error(f"Error in send2zabbix for station {soh_data[0]}: {e}")