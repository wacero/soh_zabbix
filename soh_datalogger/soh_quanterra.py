import time
import logging
from pyzabbix import ZabbixMetric, ZabbixSender
from bs4 import BeautifulSoup
import re
import subprocess


def connect2zabbix(zx_ip,zx_port):
    """
    Try to connect to a ZX server, return
    """
    try:
        return ZabbixSender(zx_ip,zx_port)
        
    except Exception as e:
        raise("Error in connect2zabbix: %s" %str(e))    







def send2Zabbix(zx_server,soh_data,rk_stat):
    """
    Receive soh_data array and send it to a ZX trigger item only  if ping ok 
    """
    
    try:
        hostname = "%s" %soh_data[0]
        if rk_stat[hostname]==True:

            hostname="%s_RK" %soh_data[0]
            metrics=[
                ZabbixMetric(hostname,'temperature',float(soh_data[1])),
                ZabbixMetric(hostname,'voltage',float(soh_data[2]),time.time()),
                ZabbixMetric(hostname,'voltage_intern',float(soh_data[3]),time.time()),
                ZabbixMetric(hostname,'delay',float(soh_data[4]),time.time()),
                ZabbixMetric(hostname,'disk_1',float(soh_data[6]),time.time()),
                ZabbixMetric(hostname,'disk_2',float(soh_data[7]),time.time())
                ]
  
            try:
                res=zx_server.send(metrics)
                logging.info("%s, %s" %(hostname,res))
            except Exception as e:
                logging.info("Error in send2Zabbix. Error was: %s" %str(e))
   
    except Exception as e:
        logging.info("Not station in dictionary: %s" %str(e))

def send_q330_http_status(zx_server,q330_http):
    
    metrics = [ 
            ZabbixMetric(q330_http['station'],'http_connection',float(q330_http['http_connection'])),
            ZabbixMetric(q330_http['station'],'http_speed',float(q330_http['http_speed']),time.time()),
            ZabbixMetric(q330_http['station'],'http_time',float(q330_http['http_time']),time.time())    
                ]
    try:
        result = zx_server.send(metrics)
        logging.info("%s" % q330_http )
        logging.info(metrics)
        logging.info(result)
    
    except Exception as e:
        logging.error("Error in send_q330_http_status")

def send_q330_soh(zx_server,q330_soh):
    metrics = [
            ZabbixMetric(q330_soh['station'],'q330_temperature',float(q330_soh['q330_temperature']),time.time()),
            ZabbixMetric(q330_soh['station'],'q330_in_voltage',float(q330_soh['q330_in_voltage']),time.time())
                ]

    try:
        result = zx_server.send(metrics)
        logging.info("%s" % q330_soh )
        logging.info(metrics)
        logging.info(result)

    except Exception as e:
        logging.error("Error in send_q330_soh: %s" %str(e))




def check_get_http(station,ip,port,zx_server):
    
    try:
        
        q330_url="http://%s:%s/stats.html" %(ip,port)
        response = subprocess.run(["wget","%s"%q330_url,"-O","/tmp/%s.html"%station ],capture_output=True,timeout=120)
        return_code=response.returncode
        print(response)

    
    except Exception as e:
        
        logging.error("Timeout in check_get_http:%s, %s,%s" %(str(e),station,ip))    
        return_code=-1

    if return_code != 0:
        http_connection=0
        http_speed = 0
        http_time = 0
            
    else:
        
        http_out=response.stderr.decode("utf-8").split("\n")
        #for i,http in enumerate(http_out):
        #    print(i,http)
        
        http_temp=http_out[6].split("%")[1]
        http_speed,http_time=http_temp.split("=")
        if "K" in http_speed:
            http_speed=float(http_speed.rstrip("K"))
            http_speed=http_speed*1000
    
        http_time=http_time.rstrip("s")        
        http_connection=1
        q330_soh=get_soh_info(station) 
        send_q330_soh(zx_server,q330_soh)
        #llamar a procesamiento de soh de stats.html     
    
    q330_http={"station":"%s_QA"%station,"http_connection":http_connection,
                        "http_speed":http_speed,"http_time":http_time}
    send_q330_http_status(zx_server,q330_http)    
    

def get_soh_info(station):
    
    try:
        station_file=open("/tmp/%s.html"%station)
        #station_info=station_file.readlines()
        station_info=BeautifulSoup(station_file,"html.parser")
        pres=station_info.find_all("pre")
        
        soh_data=pres[1].get_text()
        q330_temperature=re.search(".*System Temperature.*",soh_data)
        q330_temperature=q330_temperature.group(0).split(":")[1]
        q330_temperature=q330_temperature.rstrip("C")
        
        q330_in_voltage=re.search(".*Input Voltage.*",soh_data)
        q330_in_voltage=q330_in_voltage.group(0).split(":")[1]
        q330_in_voltage=q330_in_voltage.rstrip("V")

        q330_soh={"station":"%s_QA"%station,"q330_temperature":q330_temperature,
                   "q330_in_voltage":q330_in_voltage  }
        
        print("##%s" %q330_temperature,q330_in_voltage)
        return q330_soh
    except Exception as e:
        logging.error("Error in get_soh_info, was:%s" %str(e) )

