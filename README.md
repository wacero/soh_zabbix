This project contains python code that scraps information from Reftek, Quanterra and Guralp dataloggers and send them to a Zabbix DB. 

```
$ conda create -n soh_xabbix
$ conda install python=3.8 beautifulsoup4 pygelf requests

$ pip install py-zabbix 

```
