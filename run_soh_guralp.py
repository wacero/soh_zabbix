'''
What mortal can ward the craft of the god all-deceiving. Persas,97

@author: wacero
'''

import time
from bs4 import BeautifulSoup
import requests
from pyzabbix import ZabbixMetric, ZabbixSender
import os, sys
from soh_datalogger import soh_utils
from soh_datalogger import soh_guralp
import logging, logging.config
import subprocess
from concurrent.futures import ThreadPoolExecutor



logging.config.fileConfig('./config/logging.ini', disable_existing_loggers=False)
logger=logging.getLogger('run_soh_reftek')



def check_host_and_append(station,ip):
    if soh_utils.check_host_up(ip):
        return dict({station:ip})
    else:
        logger.info(f"host down: {station,ip}")
        return None

def fetch_soh_data(station, ip):
    try:
        return soh_guralp.get_guralp_soh(station, ip)
    except Exception as e:
        logger.error(f"Error fetching SOH for {station} ({ip}): {e}")
        return None


def main():
    """
    This function parse the configuration file, set parameters and read reftek stations
    """
     
    is_error = False
    
    if len(sys.argv)==1:
        is_error=True
    
    else:
        try:
            run_param = soh_utils.read_parameters(sys.argv[1])
        except Exception as e:
            logger.error("Error reading configuration file: %s" %str(e))
            raise Exception("Error reading configuration file: %s" %str(e))
    
        try:
            guralp_station_file = run_param['GURALP_INFO']['file_path']
            zabbix_server_ip = run_param['ZABBIX_INFO']['ip'] 
            zabbix_port = run_param['ZABBIX_INFO']['port']
        except Exception as e:
            logger.error("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))    
    
        try:
            guralp_stations = soh_utils.read_config_file(guralp_station_file)
            print(guralp_stations)
            
        except Exception as e:
            logger.error("Error getting parameters: %s" %str(e))
            raise Exception("Error getting parameters: %s" %str(e))

        try:
            zabbix_sender = soh_utils.get_zabbix_sender(zabbix_server_ip, int(zabbix_port))
            
        except Exception as e:
            logger.error("Error in main(): error getting zabbix connection %s " %str(e))

        #'''

        try:
            hosts_up = []

            with ThreadPoolExecutor(max_workers=10) as executor:
                future_to_station = {executor.submit(check_host_and_append, station, ip): station for station, ip in guralp_stations.items()}
                
                for future in future_to_station:
                    result = future.result()  # Espera a que la tarea se complete y obtiene el resultado
                    if result:
                        hosts_up.append(result)

        except Exception as e:
            logger.error(f"Error in main(): error getting GURALP stations {e}")
        

        try:
            hosts_soh = []
            with ThreadPoolExecutor(max_workers=10) as executor:
                future_to_station = {executor.submit(fetch_soh_data, station, ip): (station, ip) for station_dict in hosts_up for station, ip in station_dict.items()}
                
                for future in future_to_station:
                    temp_soh = future.result()
                    if temp_soh:
                        hosts_soh.append(temp_soh)


        except Exception as e:
            logger.error(f"Error in main(): {e}")



        try:

            for host in hosts_soh:
                soh_guralp.send2zabbix(zabbix_sender,host)

            
        except Exception as e:
            logger.error(f"Error in main(): {e}")
   

    if is_error:
        logger.info(f'Usage: python {sys.argv[0]} configuration_file.txt ')
        print(f'Usage: python {sys.argv[0]} CONFIGURATION_FILE.txt ')



if __name__ == "__main__":

    main()









